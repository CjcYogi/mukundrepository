<%@page import="org.hibernate.Query"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ taglib prefix="t" uri="http://java.sun.com/jstl/core_rt" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>User Creation</title>
</head>
<body>
	<form:form action="saveUserData" method="post" commandName="user">
			<table>
					<tr>
							<th colspan="2" align="center"><h2>CREATE USER</h2></th>
					</tr>
					<tr>
						<td>Name</td>
						<td>
							<form:input path="name" title="Enter Name"/>
						</td>
					</tr>
					<tr>
						<td>Username</td>
						<td>
							<form:input path="username" title="Enter Username" />
						</td>
					</tr>
					<tr>
						<td>Enter Password</td>
						<td>
							<form:password path="password" title="Enter Password"  id="txtPassword"/>
						</td>
					</tr>
					<tr>
						<td>Confirm Password</td>
						<td>
							<form:password path="password" title="Retype Password"  id="txtConfPassword"/>
						</td>
					</tr>
					<tr>
						<td>Email</td>
						<td>
							<form:input path="email" title="Retype Password"/>
						</td>
					</tr>
					<tr>
						<td>Contact No</td>
						<td>
							<form:input path="mobileNo" title="Contact Number"/>
						</td>
					</tr>					
					<tr>
						<td>Department</td>
						<td>
							<form:select path="departmentName"  id="ddlDepartment">
									<form:option value="" label="Select Department" />
										<t:forEach items="${DeptList}" var="dept">
											<form:option value="${dept.departmentId}" label="${dept.departmentName}"></form:option>
										</t:forEach>
										<%-- <form:options items="${DeptList}" itemLabel="departmentName" itemValue="departmentId"/> --%>
							</form:select>
						</td>
					</tr>	
					<td>Role</td>
						<td>
							<%-- <form:select path="roleName" id="ddlRole">
									<form:option value="0" label="Select Role" />
										<t:forEach items="${RoleList}" var="role">
											<form:option value="${role.roleId}" label="${role.roleName}"></form:option>
										</t:forEach>
									<form:options items="${RoleList}" itemLabel="roleName" itemValue="roleId"/>
							</form:select> --%>
							<select name="ddlRole">
									<option value="">Select Role</option>
									<t:forEach items="${RoleList}" var="role">
											<option value="${role.roleId}">${role.roleName}</option>
										</t:forEach>
							</select>
						</td>
					</tr>	
					<tr>
						<td>
								
						</td>
						<td>
								<input type="submit" value="Save"/>
								<input type="reset" value="Reset">
						</td>
					</tr>				
			</table>
			
	</form:form>
</body>
</html>