<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add Department</title>
</head>
<body>
		<center>			
				<form:form action="addDeptData" method="post" commandName="department">
						<table>
							<tr>
								<th colspan="2" align="center">ADD DEPARTMENT</th>
							</tr>
							<tr>
								<td>Role Name</td>
								<td>
									<form:input path="departmentName" name="txtDeptName"/>
								</td>
							</tr>
							<tr>
								<td colspan="2" align="right">
									<input type="submit" value="Add Department">
								</td>
							</tr>
							<tr>
								<td colspan="2" align="right">
										<a href="displayDeptData">Display Department Data</a>
								</td>
							</tr>
					</table>
				</form:form>	
				<%-- <form action="addDept" method="post" com="department">
						
				</form> --%>
		</center>
</body>
</html>