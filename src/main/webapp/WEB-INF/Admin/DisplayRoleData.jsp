<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.model.Role"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ taglib prefix="t" uri="http://java.sun.com/jstl/core_rt" %>



<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Role Data</title>
</head>
<body>
	<center>
			<form:form action="" method="post" commandName="role">
						<table border="1">
								<tr>
									<th colspan="3" align="center"><h3>Role Data</h3></th>
								</tr>
								<tr>
									<th>Role_Name</th>
									<th></th>
									<th></th>									
								</tr>
								<t:forEach items="${RoleList}" var="role">
								<tr>
									<td align="center">
														<t:out value="${role.roleName}" />
									</td>
				 					<td align="center">
				 										<a href="editRoleData?roleId=<t:out value="${role.roleId}" />">Edit</a>
				 					</td>
									<td align="center">
														<a href="deleteRoleData?roleId=<t:out value="${role.roleId}" />">Delete</a>
									</td>
								</tr>								
								</t:forEach>
						</table>
				</form:form>
	</center>
</body>
</html>