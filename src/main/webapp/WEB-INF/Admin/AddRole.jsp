<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add Role</title>
</head>
<body>
		<center>
				<h4>Add Role Page</h4>
				
				<form:form action="addRoleData" method="post" commandName="role">
					<table>
							<tr>
								<th colspan="2" align="center">ADD ROLE</th>
							</tr>
							<tr>
								<td>Role Name</td>
								<td>
									<form:input path="roleName" name="txtRoleName"/>
								</td>
							</tr>
							<tr>
									<td colspan="2" align="right">
										<input type="submit" value="Add Role">
									</td>
							</tr>
							<tr>
									<td colspan="2" align="center">
										<a href="displayRoleData">Display Role Data</a>
									</td>
							</tr>
					</table>
				</form:form>
		</center>
</body>
</html>