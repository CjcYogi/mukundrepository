<%@page import="org.hibernate.Query"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="t" uri="http://java.sun.com/jsp/jstl/core" %>



<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Department Data</title>
</head>
<body>
		<center>
				<form:form action="" method="post" commandName="department">
						<table border="1">
								<tr>
									<th colspan="3" align="center"><h3>Department Info</h3></th>
								</tr>
								<tr>
									<th>Department Name</th>
									<th></th>
									<th></th>									
								</tr>
								<t:forEach items="${DeptList}" var="dept">
								<tr>								
									<td align="center"><t:out value="${dept.departmentName}"></t:out></td>
									<td>
										<a href="editDeptData?deptId=<t:out value="${dept.departmentId}" />">Edit</a>
									</td>
									<td>
										<a href="deleteDeptData?deptId=<t:out value="${dept.departmentId}" />">Delete</a>
									</td>								
								</tr>
								</t:forEach>
						</table>
				</form:form>
		</center>
</body>
</html>