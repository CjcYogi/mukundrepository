<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    <%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Request For Change</title>
</head>
<body style="height: 400px; width: 1344px">

	<table border="1" height="500" width="700">
		<tr>
			<td colspan="2" height="20" width="500">
				<tiles:insertAttribute name="header"  />
			</td>
		</tr>

		<tr>
			<td><tiles:insertAttribute name="menu" />
			</td>
			<td><tiles:insertAttribute name="body" />
			</td>
		</tr>

		<tr>
			<td colspan="2" height="20" width="500">
				<tiles:insertAttribute name="footer" /></td>
		</tr>
	</table>

</body>
</html>