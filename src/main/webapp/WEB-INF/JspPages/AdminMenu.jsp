<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Admin Menu</title>
</head>
<body>
		<ul>
			<li><a href="createUser">Create User</a><br>
			<li><a href="viewUser">View User</a><br>
			<li><a href="createProcess">Create Process</a><br>
			<li><a href="createProcessFlow">Create Process Flow</a><br>
			<li><a href="addRole">Add Role</a><br>
			<li><a href="addDepartment">Add Department</a><br>
			<li><a href="activateUser">Activate User</a><br>
			<li><a href="logout">Logout</a>
		</ul>
</body>
</html>