<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Login Page</title>
</head>
<body>
		<center>
				<form:form action="doLogin" method="post" commandName="login">		
						<table>
								<tr>
									<th colspan="2" align="center"><h2>LOGIN</h2></th>
								</tr>
								<tr>
									<td>Enter Username</td>
									<td>
										<form:input path="username" name="txtUsername" />
									</td>
								</tr>
								<tr>
									<td>Enter Password</td>
									<td>
										<form:password path="password" name="txtPassword"/>
									</td>
								</tr>
								<tr>
									<td colspan="2" align="right">
										<input type="submit" value="Login">
									</td>
								</tr>
								<tr>
									<td colspan="2" align="right">
										<a href="forgotPassword">Forgot Password? Click Here..!</a>
									</td>
								</tr>
								<tr>
									<td>
										${msg}
									</td>
								</tr>
						</table>
				</form:form>
		</center>
</body>
</html>