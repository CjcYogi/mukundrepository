package com.service;

import java.util.List;

import com.model.Department;
import com.model.Role;
import com.model.User;

public interface RFC_Service
{
	public boolean chcekLogin(String username,String password);
	
	public boolean addRole(Role role);
	
	//public boolean addDepartment(Department department);
	
	public List getRoleData();
	
	public boolean addDepartmentData(Department department);
	
	public List getDepartmentData();
	
	public boolean saveUserData(User user);
	
	public boolean saveUserDataIntoLogin(User user);
	
	public boolean saveUserDataIntoUserRole(User user);
	
	public String getDepartNameByDeptId(int deptId);
	
	public String getRoleNameByRoleId(int roleId);
	
}
