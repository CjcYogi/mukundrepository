package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.dao.RFC_Dao;
import com.dao.Rfc_DaoImple;
import com.model.Department;
import com.model.Role;
import com.model.User;

public class RFC_ServiceImple implements RFC_Service
{
	@Autowired
	Rfc_DaoImple dao;

	public boolean chcekLogin(String username, String password) {
		//System.out.println("Entry in chcekLogin()----Service Imple");
		boolean res=false;
		try
		{
			res=dao.chcekLogin(username, password);
		}
		catch(Exception ex)
		{
			ex.toString();
		}
		return res;
	}

	public boolean addRole(Role role)
	{
		boolean result=dao.addRole(role);
		return result;
	}

	public boolean addDepartment(Department department) {
		boolean result=dao.addDeprtment(department);
		return result;
	}

	public List getRoleData() {
		List<Role> roleList=dao.getRoleData();
		return roleList;
	}

	public boolean addDepartmentData(Department department) {
		boolean result=dao.addDepartmentData(department);
		return result;
	}

	public List getDepartmentData() 
	{
		List<Department> deptList=dao.getDepartmentData();
		return deptList;
	}

	public boolean saveUserData(User user) {
		boolean result=dao.saveUserData(user);
		return result;
	}

	public boolean saveUserDataIntoLogin(User user) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean saveUserDataIntoUserRole(User user) {
		// TODO Auto-generated method stub
		return false;
	}

	public String getDepartNameByDeptId(int deptId)
	{
		String deptName=dao.getDepartNameByDeptId(deptId);
		return deptName;
	}

	public String getRoleNameByRoleId(int roleId) {
		// TODO Auto-generated method stub
		return null;
	}

	
	

}
