package com.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.model.Department;
import com.model.Login;
import com.model.Process;
import com.model.Role;
import com.model.User;
import com.service.RFC_ServiceImple;

@Controller
public class RfcController
{
		List<User> userList;
		List<Department> departmentList;
		List<Role> roleList;
	
		@Autowired
		RFC_ServiceImple service;
				
		@RequestMapping("/")
		public ModelAndView loadPage(Login login)
		{
			System.out.println("Index page is loading....");
			return new ModelAndView("loginLayout");
		}
		
		@RequestMapping(value="/doLogin")
		public ModelAndView checkUserLoginData(Model model,HttpServletRequest request,Login login)
		{
				boolean isValidUser=false;
				
				try
				{
					String username=login.getUsername();
					String password=login.getPassword();
					isValidUser=service.chcekLogin(username, password);
				}
				catch(Exception ex)
				{
					System.out.println("Error in checkLoginUserData---"+ex);
				}				
				if(isValidUser==true)
				{
					return new ModelAndView("adminHome");
				}
				else
				{
					model.addAttribute("msg","Invalid User Credential");
					return new ModelAndView("loginLayout");
				}
		}
		
			/* Opening Create User Page */
		@RequestMapping(value="/createUser")
		public ModelAndView openCreateUserPage(User user,Model model)
		{
			//System.out.println("Create user page will be load...");
			departmentList=service.getDepartmentData();
			//System.out.println("Department List Size : "+departmentList.size());
			
			roleList=service.getRoleData();
			//System.out.println("Role List Size : "+roleList.size());
			model.addAttribute("DeptList", departmentList);
			model.addAttribute("RoleList", roleList);
			return new ModelAndView("viewCreateUserPage");
		}
		
				/*Saving User Data Into DB*/
		@RequestMapping(value="/saveUserData")
		public ModelAndView saveUserData(User user,Model model,HttpServletRequest request)
		{
				System.out.println("Entry in saveUserData()-----Controller Class");
				
				int id=user.getUserId();
				String name=user.getName();
				String userName=user.getUsername();
				String password=user.getPassword();
				String pass=request.getParameter("txtConfPassword");
				System.out.println("pass "+pass);
				String email=user.getEmail();
				long mobNo=user.getMobileNo();
				String department=user.getDepartmentName();
				String role=user.getRoleName();
				int rid=Integer.parseInt(role);
				int did=Integer.parseInt(department);
				String dept=request.getParameter("ddlDepartment");
				String roleNm=request.getParameter("ddlRole");
				System.out.println(dept+"\n"+roleNm);
				System.out.println("UserID : "+id+"\nName : "+name+"\nUserName : "+userName+"\nPassword : "+password+"\nEmail : "+email+"\nConatct No : "+mobNo+"\nDepartment : "+department+"\nRole Name : "+role);
				/*boolean result=service.saveUserData(user);
				
				if(result=false)
				{
						return new ModelAndView("viewCreateUserPage");
				}
				else
				{
						return new ModelAndView("viewUserPage");
				}*/
				
				String deptName=service.getDepartNameByDeptId(did);
				System.out.println("Department Name : "+deptName);
				
				return new ModelAndView("");
		}
		
			/* Opening View User Page */
		@RequestMapping(value="/viewUser")
		public ModelAndView openViewUserPage(User user)
		{
			System.out.println("Create user page will be load...");
			
			return new ModelAndView("viewUserPage");
		}
		
		/* Opening Create Process Page */
		@RequestMapping(value="/createProcess")
		public ModelAndView openCreateProcessPage(Process process)
		{
			System.out.println("Create process page will be open shortly....");			
			return new ModelAndView("viewProcessPage");
		}
		
			/* Opening Create Process Flow Page */
		@RequestMapping(value="/createProcessFlow")
		public ModelAndView openCreateProcessFlowPage(Process process)
		{
			System.out.println("Create Process flow page will occur shortly....");			
			return new ModelAndView("viewProcessFlowPage");
		}
		
			/* Opening Add Role Page */
		@RequestMapping(value="/addRole")
		public ModelAndView openAddRolePage(Role role,HttpServletRequest request,Model model)
		{			
			System.out.println("Add role page will be open shortly....");
			return new ModelAndView("viewAddRolePage");
		}
		
		@RequestMapping(value="/addRoleData")
		public ModelAndView saveRoleData(Role role,Model model)
		{
			try
			{
				System.out.println("Entry in saveDepartmentData()------RFC_Controller");
				String roleName=role.getRoleName();
				System.out.println("Role Name : "+roleName);
				
				boolean res=service.addRole(role);
				
				roleList=service.getRoleData();
			}
			catch(Exception ex)
			{
				System.out.println("Error in saveRoleData()-----Rfc Controller");
			}
			
			if(roleList.isEmpty())
			{
				roleList.clear();
				return new ModelAndView("viewAddRolePage");
			}
			else
			{
				model.addAttribute("RoleList", roleList);
				return new ModelAndView("showRoleDataPage");
			}
	
		}
		
		@RequestMapping(value="/displayRoleData")
		public ModelAndView showRoleDataPage(Role role,Model model)
		{
			System.out.println("roleee");
			roleList=service.getRoleData();
			if(roleList.isEmpty())
			{
				return new ModelAndView("viewAddRolePage");
			}
			else
			{
				model.addAttribute("RoleList", roleList);
				return new ModelAndView("showRoleDataPage");
			}
		}
			
				/* Opening Create Department Page */
		@RequestMapping(value="/addDepartment")
		public ModelAndView openCreateDepartmentPage(Department department)
		{
			System.out.println("Opening create department page shortly....");
			return new ModelAndView("viewAddDepartmentPage");
		}
		
		/* Saving Department Data Into DB*/	
		@RequestMapping(value="/addDeptData")
		public ModelAndView saveDepartmentData(Department department,Model model)
		{
			List<Department> departmentList=new ArrayList<Department>();
			System.out.println("Entry in saveDepartmentData()------RFC_Controller");
			
			String deptName=department.getDepartmentName();
			System.out.println("Department Name is : "+deptName);
			
			boolean res=service.addDepartment(department);
			
			departmentList=service.getDepartmentData();
			
		
				return new ModelAndView("showDeptDataPage");
		}
		
		@RequestMapping(value="/displayDeptData")
		public ModelAndView showDepartmentDataPage(Department department,Model model)
		{
			departmentList=service.getDepartmentData();
			
			if(departmentList.isEmpty())
			{
				departmentList.clear();
				model.addAttribute("DeptList",departmentList);
				return new ModelAndView("viewAddDepartmentPage");
			}
			else
				model.addAttribute("DeptList", departmentList);
				return new ModelAndView("showDeptDataPage");
		}
				
			/* Opening Activate User Page */
		@RequestMapping(value="/activateUser")
		public ModelAndView openActivateUserForm(User user)
		{
			System.out.println("Activate user page will be open shortly...");
			
			return new ModelAndView("viewActivateUserPage");
		}
		
				/* Saving data into Department Table */
		@RequestMapping(value="/addDept")
		public ModelAndView saveDepartmentData(Department department,HttpServletRequest request,BindingResult bindingResult)
		{
			String deptName=department.getDepartmentName();
			System.out.println("Department Name is : "+deptName);
			boolean result=service.addDepartment(department);
			
			return new ModelAndView("addDepartmentForm");
		}		
		
		
		public ModelAndView showRoleData(Model model)
		{
			List<Role> rList=service.getRoleData();
			if(!rList.isEmpty())
			{
				System.out.println("Data is present");
				model.addAttribute("RoleList", rList);
			}
			else
			{
				rList.clear();
				model.addAttribute("RoleList", rList);
				System.out.println("Data is not present in db");
			}
			return new ModelAndView("showRoleDataPage");
		}	
		
}
		 

