package com.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.model.Department;
import com.model.Role;
import com.model.User;

public class Rfc_DaoImple implements RFC_Dao
{
	@Autowired
	SessionFactory sessionFactory;
	Session session;

	public boolean chcekLogin(String username, String password)
	{
		//System.out.println("Entry in chcekLogin()----Dao Imple");
		boolean res=false;
		try
		{
			session=sessionFactory.openSession();
			session.beginTransaction();
			
			String validateSql="select username,password from Login where username=:unm and password=:psw";
					
			Query query=session.createQuery(validateSql);
			query.setParameter("unm", username);
			query.setParameter("psw", password);
			
			List<User> userList=query.list();
				if(userList.isEmpty())
				{
					res=false;
					return res;
				}
				else
				{
					res=true;
					return res;
				}
		}
		catch(Exception ex)
		{
			System.out.println(ex);
		}
		return res;
	}

	public boolean addRole(Role role) 
	{
		session=sessionFactory.openSession();
		session.beginTransaction();
		
		boolean result=false;
		System.out.println("Entry in addRole()---Dao Implementation");
		try
		{
			session.save(role);
			session.beginTransaction().commit();
			session.flush();
			result=true;
		}
		catch(Exception ex)
		{
			System.out.println("Error in addRole()---Dao Imple \n"+ex);
		}
		
		return result;
	}

	public boolean addDeprtment(Department department) 
	{
		session=sessionFactory.openSession();
		session.beginTransaction();
		boolean result=false;
		System.out.println("Entry in addDepartment()---Dao Implementation");
		try
		{
			session.save(department);
			session.beginTransaction().commit();
			session.flush();
			result=true;
		}
		catch(Exception ex)
		{
			System.out.println("Error in addRole()---Dao Imple \n"+ex);
		}
		return result;
	}
	
	
	public List getRoleData()
	{ 
		List<Role> roleList=new ArrayList<Role>();
		
		try
		{
			session=sessionFactory.openSession();
			session.beginTransaction();
			
			String selectRoleData="from Role";
			
			Query query=session.createQuery(selectRoleData);
			
			roleList=query.list();
			
			if(!roleList.isEmpty())
			{
				return roleList;
				//System.out.println("Data present in database");
			}
			else
			{
				roleList.clear();
				return roleList;
				//System.out.println("Data not present in database");
			}
		}
		catch(HibernateException he)
		{
			System.out.println("Error in getRoleData()-----DaoImple "+he);
		}
		
		return roleList;
	}

	public boolean addDepartmentData(Department department)
	{
		boolean result=false;
		
		session=sessionFactory.openSession();
		session.beginTransaction();
		session.save(department);
		session.beginTransaction().commit();
		session.flush();
		result=true;
		System.out.println("Result : "+result);
		return result;
	}

	public List getDepartmentData() {
		List<Department> departmentList=new ArrayList<Department>();
		
		try
		{
			session=sessionFactory.openSession();
			session.beginTransaction();
			
			String selectRoleData="from Department";
			
			Query query=session.createQuery(selectRoleData);
			
			departmentList=query.list();
			
			if(!departmentList.isEmpty())
			{
				//System.out.println("Data present in database");
			}
			else
			{
				departmentList.clear();
				return departmentList;
				//System.out.println("Data not present in database");
			}
		}
		catch(HibernateException he)
		{
			System.out.println("Error in getRoleData()-----DaoImple "+he);
		}
		//System.out.println("IS department list is empty??? "+departmentList.isEmpty());
		//System.out.println("Size of list is : "+departmentList.size());
		return departmentList;
	}

	public boolean saveUserData(User user)
	{
		boolean result=false;
		System.out.println("Entry in saveUserData()----Dao Implementation");
		try
		{
			int id=user.getUserId();
			String name=user.getName();
			String userName=user.getUsername();
			String password=user.getPassword();
			String email=user.getEmail();
			long mobNo=user.getMobileNo();
			String department=user.getDepartmentName();
			String role=user.getRoleName();
			
			System.out.println("UserID : "+id+"\nName : "+name+"\nUserName : "+userName+"\nPassword : "+password+"\nEmail : "+email+"\nConatct No : "+mobNo+"\nDepartment : ");
			
			session=sessionFactory.openSession();
			session.beginTransaction();
			session.save(user);
			
			result=true;
			//return result;
		}
		catch(Exception ex)
		{
			System.out.println("Error in saveUserData()----Dao Implementation"+ex);
		}
		return result;
	}

	public boolean saveUserDataIntoLogin(User user) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean saveUserDataIntoUserRole(User user) {
		// TODO Auto-generated method stub
		return false;
	}

	public String getDepartNameByDeptId(int deptId)
	{
		String deptName="";
		session=sessionFactory.openSession();
		session.beginTransaction();
		System.out.println("Entry in getDepartNameByDeptId()------Dao Imple Clas");
		
		try
		{
			String getDeptName="select departmentName from Department where departmentId=:deptId";
			Query query=session.createQuery(getDeptName);
			query.setParameter("departmentId", deptId);
			
			List<String> dept=new ArrayList<String>();
			
			dept=query.list();
			if(!dept.isEmpty())
			{
				System.out.println("Dept name is available in list");
				deptName=dept.get(0);
				System.out.println("Department Name is : "+deptName);
			}
			else
			{
				System.out.println("No dept name is present");
				deptName=null;
			}
		}
		catch(Exception ex)
		{
			System.out.println("Error in getDepartNameByDeptId()-----Dao Imple"+ex);
		}
		return deptName;
	}

	public String getRoleNameByRoleId(int roleId) {
		// TODO Auto-generated method stub
		return null;
	}

	
}
