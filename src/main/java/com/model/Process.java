package com.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="RFC_Process")
public class Process 
{
		@Id
		@Column(name="ProcessId")
		@GeneratedValue(strategy=GenerationType.IDENTITY)
		private int processId;
		
		@Column(name="ProcessName")
		private String processName;

		public int getProcessId() {
			return processId;
		}
		public void setProcessId(int processId) {
			this.processId = processId;
		}

		public String getProcessName() {
			return processName;
		}
		public void setProcessName(String processName) {
			this.processName = processName;
		}
		
		
}
