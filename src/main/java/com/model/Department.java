package com.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="RFC_Department")
public class Department 
{		
		@Id
		@GeneratedValue(strategy=GenerationType.IDENTITY)
		@Column(name="Department_ID")
		private int departmentId;
		
		@Column(name="DeptName")
		private String departmentName;

		public int getDepartmentId() {
			return departmentId;
		}
		public void setDepartmentId(int departmentId) {
			this.departmentId = departmentId;
		}

		public String getDepartmentName() {
			return departmentName;
		}
		public void setDepartmentName(String departmentName) {
			this.departmentName = departmentName;
		}		
}
