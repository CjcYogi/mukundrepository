package com.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="RFC_User")
public class User 
{
		@Id
		@GeneratedValue(strategy=GenerationType.IDENTITY)
	    @Column(name="User_ID")
		private int userId;
		
		@Column(name="Name")
		private String name;
		
		@Column(name="UserName")
		private String username;
		
		@Column(name="Password")
		private String password;
		
		@Column(name="Email")
		private String email;
		
		@Column(name="Contact_No")
		private long mobileNo;
		
		/*@OneToOne(targetEntity=Department.class,cascade=CascadeType.ALL)
		@JoinColumn(name="deptname",referencedColumnName="Department_Name")*/
		
		@Column(name="DeaprtmentName")
		private String departmentName;
		
		/*@OneToOne(targetEntity=UserRole.class,cascade=CascadeType.ALL)
		@JoinColumn(name="userId",referencedColumnName="UserID")*/
		/*@Column(name="UserRole")
		private UserRole userRole;*/
		
		@Column(name="User_Role_Name")
		private String roleName;
		
		

		public String getRoleName() {
			return roleName;
		}
		public void setRoleName(String roleName) {
			this.roleName = roleName;
		}
		public int getUserId() {
			return userId;
		}
		public void setUserId(int userId) {
			this.userId = userId;
		}

		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}

		public String getUsername() {
			return username;
		}
		public void setUsername(String username) {
			this.username = username;
		}

		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
		}

		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}

		public long getMobileNo() 
		{
			return mobileNo;
		}
		public void setMobileNo(long mobileNo) 
		{
			this.mobileNo = mobileNo;
		}
		
		public String getDepartmentName() {
			return departmentName;
		}
		public void setDepartmentName(String departmentName) {
			this.departmentName = departmentName;
		}		
}
