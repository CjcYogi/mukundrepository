package com.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="RFC_UserRole")
public class UserRole
{
		@Id
		@Column(name="UserRole")
		@GeneratedValue(strategy=GenerationType.IDENTITY)
		private int userRoleId;
		
		@Column(name="RoleId")
		private int roleId;
		
		@Column(name="UserId")
		private int userId;

		public int getUserRoleId() {
			return userRoleId;
		}
		public void setUserRoleId(int userRoleId) {
			this.userRoleId = userRoleId;
		}

		public int getRoleId() {
			return roleId;
		}
		public void setRoleId(int roleId) {
			this.roleId = roleId;
		}

		public int getUserId() {
			return userId;
		}
		public void setUserId(int userId) {
			this.userId = userId;
		}		
}
