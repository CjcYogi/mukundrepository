package com.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="RFC_Login")
public class Login
{
		@Id
		@Column(name="LoginId")
		@GeneratedValue(strategy=GenerationType.IDENTITY)
		private int loginId;
		
		@Column(name="Flag")
		private boolean flag;
		
		@Column(name="Last_login_Time")
		private Date last_login_time;
		
		@Column(name="Password_Change_Time")
		private Date psw_change_time;
		
		@Column(name="Login_Failed_Attempt")
		private int login_failed_attempt;
		
	/*	@Column(name="UserId")
		private User user;*/
		
		@Column(name="Username")
		private String username;
		

		public String getUsername() {
			return username;
		}
		public void setUsername(String username) {
			this.username = username;
		}
		public int getLoginId() {
			return loginId;
		}
		public void setLoginId(int loginId) {
			this.loginId = loginId;
		}

		public boolean isFlag() {
			return flag;
		}
		public void setFlag(boolean flag) {
			this.flag = flag;
		}

		public Date getLast_login_time() {
			return last_login_time;
		}
		public void setLast_login_time(Date last_login_time) {
			this.last_login_time = last_login_time;
		}

		public Date getPsw_change_time() {
			return psw_change_time;
		}
		public void setPsw_change_time(Date psw_change_time) {
			this.psw_change_time = psw_change_time;
		}

		public int getLogin_failed_attempt() {
			return login_failed_attempt;
		}
		public void setLogin_failed_attempt(int login_failed_attempt) {
			this.login_failed_attempt = login_failed_attempt;
		}
		
		@Column(name="Password")
		private String password;

		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
		}
		
		

		/*public User getUser() {
			return user;
		}
		public void setUser(User user) {
			this.user = user;
		}	*/	
}
